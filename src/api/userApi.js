const express = require('express')
const router = express.Router()
const userData = require('../Data/data.json')
const connectToDB2 = require('../utlities/db')
const sql = require('mssql')

router.get('/DashBoard', (req,res)=> {
    
        if(userData && userData.data){
            res.json({ userData })
        }else{
            res.json({err:"no data found"})
        }
    
})

router.post('/RiderDetail', async (req,res)=> {
     const {id} = req.body

     const riderid = id
   try {
      let checkConnection = await sql.connect(connectToDB2)
     if (checkConnection) {
        const response = await sql.query`select * from ufnGetRiderPickupDtls(${riderid})`
        
        if(response.recordsets[0].length !== 0){
          res.status(200).json({msg:response})
        }else{
          res.status(404).json({err:"rider detail not found"})
        }

     }
  
   } catch (err) {
     
  }
    
})


module.exports = router