const express = require("express");
const cors = require("cors");
var multipart = require('connect-multiparty');
const User_Api = require("./api/userApi");
const Menu_Details_Api = require('./api/menuDetailApi')

const app = express();
app.use(express.json({limit: '50MB', extended: true}));
app.use(express.urlencoded({limit: '50MB', extended: true }));
app.use(cors());
app.use(multipart({
    maxFieldsSize: '50MB'
}));


app.use("/maxExchange",User_Api,Menu_Details_Api)

module.exports = app;