const sql = require('mssql');
const config = require('../config.json')


const connectToDB =  async () => {
    try {
       let checkConnection = await sql.connect(config)
   
       if (checkConnection) {
         console.log('database connected')
         return 'db'
       }else{
           console.log('connection err')
       }
       
    } catch (err) {
        res.json({err:"something went wrong"})
    }
}


// const connectToDB2 =  async () => {
//     try {
//        let checkConnection = await sql.connect(config)
   
//        if (checkConnection) {
//        }else{
//          console.log('connection err')
//        }
       
//     } catch (err) {
//         res.json({err:"something went wrong"})
//     }
// }

module.exports = connectToDB